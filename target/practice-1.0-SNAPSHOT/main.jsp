<%@ page import="javax.persistence.EntityManagerFactory" %>
<%@ page import="javax.persistence.Persistence" %>
<%@ page import="javax.persistence.EntityManager" %>
<%@ page import="javax.persistence.EntityTransaction" %>
<%@ page import="com.example.practice.AnimeBean" %><%--
  Created by IntelliJ IDEA.
  User: SarTa
  Date: 01.11.2021
  Time: 19:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<%

    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");

    EntityManager entityManager = entityManagerFactory.createEntityManager();

    EntityTransaction entityTransaction = entityManager.getTransaction();

    entityTransaction.begin();


    Cookie anime_title =  new Cookie("anime_title", request.getParameter("Anime_title"));
    Cookie release_year =  new Cookie("release_year", request.getParameter("Release_year"));
    Cookie based_on =  new Cookie("based_on", request.getParameter("Based_on"));
    Cookie studio =  new Cookie("studio", request.getParameter("Studio"));
    Cookie author=  new Cookie("author", request.getParameter("Author"));

    anime_title.setMaxAge(60 * 60 * 24);
    release_year.setMaxAge(60 * 60 * 24);
    based_on.setMaxAge(60 * 60 * 24);
    studio.setMaxAge(60 * 60 * 24);
    author.setMaxAge(60 * 60 * 24);

    response.addCookie(anime_title);
    response.addCookie(release_year);
    response.addCookie(based_on);
    response.addCookie(studio);
    response.addCookie(author);

%>
<html>
<head>
    <title>Title</title>
</head>
<body>
  <h1>REVIEW</h1>



  <ul>
      <li>
          <p>Anime Title : <%= request.getParameter("Anime_title")%>
          </p>
      </li>

      <li>
          <p>Release Year : <%= request.getParameter("Release_year")%>
          </p>
      </li>

      <li>
          <p>Studio : <%= request.getParameter("Studio")%>
          </p>
      </li>

      <li>
          <p>Anime is base on: <%= request.getParameter("Based_on")%>
          </p>
      </li>

      <li>
          <p>Author : <%= request.getParameter("Author")%>
          </p>
      </li>
  </ul>

  <form action="index.jsp" method="post" target="_balnk">
      Anime Title: <input type="text" name="Anime_title" value='<%= request.getParameter("Anime_title")%>'>
      <br>
      Release Year: <input type="number" name="Release_year" value='<%= request.getParameter("Release_year")%>'>
      <br>
      Studio: <input type="text" name="Studio" value='<%= request.getParameter("Studio")%>'>
      <br>
      Anime is based on: <input type="text" name="Based_on" value='<%= request.getParameter("Based_on")%>'>
      <br>
      Author: <input type="text" name="Author" value='<%= request.getParameter("Author")%>'>
      <br>
      <input type="submit" value="save">
  </form>



</body>
</html>
