import com.example.practice.AnimeBean;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class unitTest {

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;
    private static EntityTransaction entityTransaction;

    @BeforeAll
    static void beforeAll() {

        entityManagerFactory = Persistence.createEntityManagerFactory("default");
        entityManager = entityManagerFactory.createEntityManager();
        entityTransaction = entityManager.getTransaction();

    }

    @BeforeEach
    void beforeEach() {

        entityTransaction.begin();

    }

    @Test
    void Test(){

        AnimeBean anime = new AnimeBean();

        anime.setAuthor("Kishimoto");
        anime.setStudio("Pierrot");
        anime.setYear(2003);
        anime.setName("naruto");
        anime.setBased_on("manga");

        entityManager.persist(anime);



    }

    static AnimeBean anime() {
        return new AnimeBean(1,"naruto",2003,"pierrot","manga","kishimoto");
    }

    @ParameterizedTest
    @MethodSource("anime")
    void test_MethodSource_Objects(AnimeBean b) {
        assertNotNull(b.getYear());
    }


    @AfterEach
    void afterEach() {


        entityTransaction.commit();

    }

    @AfterAll
    static void afterAll() {


        entityManager.close();
        entityManagerFactory.close();

    }

}
