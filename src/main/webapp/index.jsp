<%@ page import="javax.persistence.EntityManagerFactory" %>
<%@ page import="javax.persistence.Persistence" %>
<%@ page import="javax.persistence.EntityManager" %>
<%@ page import="javax.persistence.EntityTransaction" %>
<%@ page import="com.example.practice.AnimeBean" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<%

    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");

    EntityManager entityManager = entityManagerFactory.createEntityManager();

    EntityTransaction entityTransaction = entityManager.getTransaction();

    try {
        entityTransaction.begin();
        if(request.getParameter("Release_year")!=null) {
            AnimeBean anime = new AnimeBean();
            anime.setName(request.getParameter("Anime_title"));
            String year = request.getParameter("Release_year");
                anime.setYear(Long.valueOf(year));
            anime.setBased_on(request.getParameter("Based_on"));
            anime.setStudio(request.getParameter("Studio"));
            anime.setAuthor(request.getParameter("Author"));

            entityManager.persist(anime);
            entityTransaction.commit();
        }
    } finally {
        if (entityTransaction.isActive()) {
            entityTransaction.rollback();
        }
        entityManager.close();
        entityManagerFactory.close();
    }

%>
<html>
<head>
</head>
<body>
<h1><%= "Hello World!" %>
</h1>
<br/>
<h1>enter anime data Senpai!</h1>
<form action="main.jsp" method="post" target="">
    Anime Title: <input type="text" name="Anime_title">
    <br>
    Release Year: <input type="number" name="Release_year">
    <br>
    Studio: <input type="text" name="Studio">
    <br>
    Anime is based on: <input type="text" name="Based_on">
    <br>
    Author: <input type="text" name="Author">
    <br>
    <input type="submit" value="Submit">
</form>

</body>
</html>