package com.example.practice;


import javax.persistence.*;
@Entity
@Table(name = "ANIME")
public class AnimeBean {
    public AnimeBean(long id, String title, long year, String studio, String based_on, String author) {
        this.id = id;
        this.title = title;
        this.year = year;
        Studio = studio;
        Based_on = based_on;
        Author = author;
    }

    public  AnimeBean(){};
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private long id;

    @Column(name="ANIME_TITLE", nullable = false, length = 20)
    private String title;

    @Column(name="YEAR", nullable = false, length = 10)
    private long year;

    @Column(name="STUDIO", nullable = false, length= 20)
    private String Studio;

    @Column(name="BASED_ON", nullable = false, length =30)
    private String Based_on;

    @Column(name="Author", nullable = false, length = 30)
    private String Author;

    public long getId() {
        return id;
    }

    public String getName() {
        return title;
    }

    public long getYear() {
        return year;
    }

    public String getStudio() {
        return Studio;
    }

    public String getBased_on() {
        return Based_on;
    }

    public String getAuthor() {
        return Author;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.title = name;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public void setStudio(String studio) {
        Studio = studio;
    }

    public void setBased_on(String based_on) {
        Based_on = based_on;
    }

    public void setAuthor(String author) {
        Author = author;
    }
}
